/*1. Any reason "+A..." is smaller than "C" as in below sorted array. Prove using javascript.

["+AZ000", "-*Z123A", "C", "D", "E", "T", "U", "X"]
*/
console.log("*************1st Qs**************");
let sortData = ["+CZ000", "-*Z123A", "+A", "D", "E", "T", "U", "X"];
let firstData = sortData[0];
let thirdData = sortData[2];
console.log(firstData.charCodeAt(0));
console.log(firstData.charCodeAt(1));
console.log(thirdData.charCodeAt(0));
console.log(thirdData.charCodeAt(1));

if(firstData.charCodeAt(0) < thirdData.charCodeAt(0)) {
    console.log(`${firstData} am smaller than ${thirdData}`);
} else {
    console.log(`${thirdData} am smaller than ${firstData}`);
}

 
// 2. What is the difference between a method & a function  in javascript ?

/* both method and functions contains a set of statements to do some task. we do wrong functions with function keywork in normal js,
method doesnt have any keyword and we do write methods in class*/

/*3. Write a function that calculates and returns the area square of an array of  rectangles. 
Formula: Area Square = length * width.
Input:
rectangles = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]

Expected output: [ 200, 200,  400, 400]
*/

console.log("*************3rd Qs**************");
function multiply1() {
    let rectangles = [ [10, 20],  ["10",20], ["A", 20] , ["ABCD", 20] ];
    let multiplyData = rectangles.map((item) => {
        return  item[0] * item[1];
        
    }).filter((item1) => {
        return item1;
    })
    
    console.log(multiplyData);
}

multiply1()
// Another way
function mutiply() {
    
    rectangles = [ [10, 20],  ["10",20], ["A20", 20] , ["A2B0CD", 20] ];
    multiplyData = [];
    rectangles.forEach((item) => {
        var x = 1;
      for(let i = 0; i < item.length; i++) {
          if(isNaN(item[i])) {
           let data = extractNumber(item[i]);
            if(data === undefined) {
                x = undefined;
                break;
            } else {
              let y = data;
            }
          }
          else {
              y = item[i];
          }
          x = x * y;
      }
        if(x !== undefined) {
            multiplyData.push(x);
        }
    })
    console.log(multiplyData);
}

function extractNumber(data) {
    // errorMessage = {
    //     error: 'there are no numarical strings and non numarical strings cant be multiplied'
    // }
    splitData = data.split('');
    extractNumberData = " ";
    for(let i = 0; i < splitData.length; i++) {
        if(!(isNaN(splitData[i]))) {
            extractNumberData += splitData[i];
            
        }
    }
    if(extractNumberData !== ' ') {
        return extractNumberData;
    }
}

mutiply();

// 4. Write a function that converts a number into a string
console.log("*************4th Qs**************");
cost = 10000;  // y it is throwing compilation error******************************************************
console.log(cost.toString());
console.log(typeof cost.toString());

function convertToString(cost = 10000) {
    var data = `${cost}`;
    console.log(data);
    console.log(typeof data);
}
convertToString();


/*5. Write a function that converts an array of array to an array of strings without duplicates.

input: [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]
output (order does not matter):  ["10", "A20", "20" , "ABCD20"] */
console.log("*************5th Qs**************");
let duplicateData = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ];
var a = [];
duplicateData.forEach((item) => {
    item.forEach((item1) => {
        a.push(item1);
        
    })
});

// console.log(a);
function removeDuplicate(a) {
   var removeduplicateData = [];
   for(let i = 0; i < a.length; i++) {
       if((removeduplicateData.indexOf(parseInt(a[i])) === -1)) {
           removeduplicateData.push(a[i]);
       }
       
   }
   console.log(removeduplicateData); 
}

removeDuplicate(a);


// How to trim the space inbetween in a single word
console.log('*********previous assignment********');
name = "   jhdsbjs jkdn kjns   "
console.log(name);
console.log(name.split(' ').join(''));

