class Calculate {
    name = "Vara"; // private variable
    // const PI = 3.14;
    constructor(firstNum = 10, secondNum = 20, thirdNum = 30) {
        this.firstNum = firstNum;
        this.secondNum = secondNum;
        this.thirdNum = thirdNum;
    }

    addSum() {
        var additionOfThreeNumbers = this.firstNum + this.secondNum + this.thirdNum;
        return additionOfThreeNumbers;
    }

    addNumbers() {
        // 7th qs
        var penHolder = ["refil", "cap", "spacePen", "pencil", "clip", "eraser", "nib", "metalHolder",
            "markers", "sketches", "10", 20, "100", 0.5, "A", "B", "C", "D", "stapler", 200, 200.5, "a", "b",
            "c"];
        var totalMoneyInPenholder = 0;
        for (var i = 0; i < penHolder.length; i++) {
            
            if( typeof penHolder[i] === 'number') {
                totalMoneyInPenholder = totalMoneyInPenholder + penHolder[i];
            } else if(parseInt(penHolder[i])){
                totalMoneyInPenholder = totalMoneyInPenholder + parseInt(penHolder[i]);
            }
        }
        console.log(totalMoneyInPenholder);

    }

    assendingOrder(alphabets) {
        // 5th and 6th
        var alphabets = alphabets;
        for(var i = 0; i < alphabets.length;i++) {
            for(var j=i+1; j<alphabets.length; j++) {
                if(alphabets[i] > alphabets[j]) {
                    var temp = alphabets[i]
                    alphabets[i] = alphabets[j]
                    alphabets[j] = temp;
                } else {
                    alphabets[i] = alphabets[i];
                    alphabets[j] = alphabets[j];
                }
            }
        }
        console.log(alphabets);
    }

    combinationArray() {
        // 4th
        var vowels = ['a', 'e', 'i', 'o', 'u'];
        var name = "shyamala";
        var intergerValues = [0,1,2,3,4,5,6,7,8,9];
        var combinedArray = vowels + ',' + intergerValues;
        console.log(combinedArray);
        //1 st
        console.log(name.charAt(9));  // returns empty string
        //2nd 
        console.log(name[9]); // returns undefined
        // 3rd qs
        // name[0] = "d";
        // console.log(name[0]); 
        // prints "s" because strings r immutable


    }
}

var obj = new Calculate();
obj.addNumbers();
obj.assendingOrder(['A100', 'A', 'C200', 'D001', 'Z100', 'z001', 'X900', 'T900',
'U98', 'a100']);
obj.assendingOrder(['A', 'E', 'C', 'D', 'Z', 'X', 'T', 'U']);
obj.combinationArray();

// OOps concepts
var obj1 = new Calculate(100, 450, 500);
console.log(obj1.addSum());

class anotheradd extends Calculate {
   data = 12;
   constructor() {
       super();
       this.thirdNum = "marks"
   }
    addSum() {
        var sumOf = this.firstNum + this.secondNum + this.thirdNum;
        return sumOf;
    }
}

var _anotheradd = new anotheradd();
console.log(_anotheradd.addSum());
console.log(_anotheradd.name); // its printing name now