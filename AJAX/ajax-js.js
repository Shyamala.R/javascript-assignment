callToServer('https://jsonplaceholder.typicode.com/photos/65', myFunction1, 'GET');
callToServer('https://jsonplaceholder.typicode.com/photos/66', myFunction2, 'GET');

function callToServer(url, myFunction, method) {
    var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        // console.log(this.readyState);
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            myFunction(this)
        }
    };

    xhttp.open(method, url);
    // console.log(this.getRequestHeader('cache-control'));
    xhttp.send();


}
function myFunction1(a) {
    console.log('Hello am function1');
    console.log(a.getAllResponseHeaders());
}

function myFunction2(a) {
    console.log('Hello am function2');
    console.log(a.getResponseHeader('content-type'));
}