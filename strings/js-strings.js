// String Methods
var name = 'Shyamala';
console.log(name.toUpperCase());
console.log(name.toLowerCase());
console.log(name.split('')); // converts the string into Array
console.log(name.slice(2, 6));
console.log(name.slice(6, 3));  // find answer - > its printing empty string****************
console.log(name.slice(-3, -1));
console.log(name.substring(2, 5)); // it will not accept negaive indexes
console.log(name.substr(4, 8)); // second argument is length
console.log(name.charAt(0));
console.log(name.charCodeAt(0));
console.log('  Hello Team Mates '.trim()); // how to trim space inbetween*****************
console.log('Hello '.concat(name));
console.log(name.length);
console.log(name.indexOf('mala', 9));
console.log(name.search('yam'));
console.log(name.lastIndexOf('mala', 3));
console.log(name.replace('mala', 'nala'));
console.log(name.startsWith('Shya')); // returns false because string is not started with 'yam'
console.log(name.includes('yan')); //returns false 
console.log(name.padEnd(15, 'S'));//pads at the end of string
console.log(name.padStart(15, 'S'));
console.log(name.repeat(3));
console.log(name.link('google.com')); //to create a link