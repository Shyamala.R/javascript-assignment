//Static variable and method
class Television{
    static screenType = 'LED';
    static initialProcedure() {
        console.log("initial Procedure");
    }

}

var tvObj = new Television();
// console.log(tvObj.screenType); //prints 'undefined' since the variable 'screenType' is static
// tvObj.initialProcedure(); //throws error

Television.initialProcedure(); // static property and methods can be accesed only with class Name , cant be accesed with objects
console.log(Television.screenType);


class AnotherTv extends Television{
  

}
AnotherTv.initialProcedure();



//private method and variable

    // class Laptop{
    //     #ramSize = 4;
    //     #hardDisk = 1;
    //     #extendMemory(type = 'HARD DISK',memory = 5, cost = 5000) {
    //         if(type === 'RAM') {
    //             let totalMemory = this.#ramSize + memory;
    //             console.log(`${type} ${totalMemory} GB costs ${cost}`);
    //         } else if(type === 'HARD DISK') {
    //             let totalMemory = this.#hardDisk + memory;
    //             console.log(`${type} ${totalMemory} TB costs ${cost}`);
    //         } else {
    //             console.log('Memory Type is wrong');
    //         }
    //     }

    //     features(type,memory, cost) {
    //         #extendMemory(type,memory, cost)
    //     }
    // }

    // var obj = new Laptop();
    // obj.features('RAM',2, 2000);