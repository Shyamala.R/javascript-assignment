function callToWebServer(method, url, data) {

    var xhttp = new XMLHttpRequest();
    xhttp.open(method, url);
    xhttp.setRequestHeader('content-type', 'application/json; charset=UTF-8')
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && (this.status === 200 || this.status === 201)) {
            console.log(`${method} output : ${this.responseText}`);
           let apendTag = document.createElement('DIV');
           let apendbr = document.createElement('BR');
           document.body.appendChild(apendTag);
           document.body.insertBefore(apendbr, apendTag);
           apendTag.innerHTML = `Method ${method} ,Status: ${this.status}: ${this.responseText}`;
            
        } 
        
    }
    xhttp.addEventListener("loadstart", handleEvent);
    xhttp.addEventListener("loadend", handleEvent);
    xhttp.addEventListener("progress", handleEvent);
    xhttp.addEventListener("load", handleEvent);
    xhttp.addEventListener("error", handleEvent);
    xhttp.addEventListener("abort", handleEvent);
    xhttp.addEventListener("timeout", handleEvent);


    // xhttp.timeout = handleEvent;
    // xhttp.loadstart = handleEvent;// its no working

    if (data == undefined) {
        xhttp.send();
    } else {
        xhttp.send(JSON.stringify(data));  // i have changed to string else it ia throwing an error
    }


}

function handleEvent(event) {
    console.log(`Hi I am ${event.type} ,I have been called`);
}

callToWebServer('GET', 'https://jsonplaceholder.typicode.com/posts/1', undefined);
callToWebServer('POST', 'https://jsonplaceholder.typicode.com/posts', { "id": 1, "title": "sdjhf" });
callToWebServer('PUT', 'https://jsonplaceholder.typicode.com/posts/2', { "id": 1, "title": "hsbajhb" });
callToWebServer('DELETE', 'https://jsonplaceholder.typicode.com/posts/1', undefined);
callToWebServer('PATCH', 'https://jsonplaceholder.typicode.com/posts/1', { "id": 1, "title": "Shyamala", "body":"Mean Stack course" });
callToWebServer('GET', 'https://jsonplaceholder.typicode.com/posts/2', undefined);