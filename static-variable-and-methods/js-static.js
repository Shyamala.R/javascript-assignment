//Static variable and method
class Television{
    static screenType = 'LED';
    static initialProcedure() {
        console.log("initial Procedure");
    }

}

var tvObj = new Television();
// console.log(tvObj.screenType); //prints 'undefined' since the variable 'screenType' is static
// tvObj.initialProcedure(); //throws error

Television.initialProcedure(); // static property and methods can be accesed only with class Name , cant be accesed with objects
console.log(Television.screenType);


class AnotherTv extends Television{
  

}
AnotherTv.initialProcedure();


