// Array methods
var data = ['Shyamala', 'Chinna', 'Chenchu', 450, 852];
console.log(data.length);
console.log(data.concat([1, 2, 3]));
console.log(data.entries().next().value); // check this **********************
console.log(data.push('eight'));
console.log(data.pop());
console.log(data.join());
console.log(data.join(' * '));
console.log(data.shift());
console.log(data.unshift('vara', 'babu'));
console.log(data.toString());
console.log(data.splice(1, 1, 'Shyamala'));
console.log(data.slice(1, 5));
console.log(data.sort());
console.log(data.sort().reverse());
console.log(data.includes('Shyamala'));
data.forEach((item, index) => {
    console.log(`Hello item No ${index} = ${item}`);
})

data.filter((item) => {
    if (!(isNaN(parseInt(item)))) {
        console.log(item);
    }
})

data.map((item) => {
    console.log(item + 2);
})

console.log(data.indexOf('Chinna'));
console.log(data.lastIndexOf('Chinna', 1))
console.log(data.findIndex((item) => {
    return item === 'Chinna';
}));


// data.some((item) => {
//    return console.log(item === 450);
// })

// data.every((item) => {
//     if(isNaN(parseFloat(item)))  {
//         console.log(item);
//     }
// })

